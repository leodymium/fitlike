# -*- coding: utf-8 -*-
"""
Created on Fri Mar  5 09:27:16 2021

@author: sambourg, Lionel Broche
"""

import numpy as np
from fitlike.ProcessingFunction import Zone2Disp, ElementWithFit


class ZoneBiExp(Zone2Disp, ElementWithFit):

    def __init__(self, meas_db):
        super().__init__(meas_db)
        self.function_name = 'Bi-exponential decay'
        self.x_label = ['Larmor Frequency (Hz)']
        self.y_label =       ['Amplitude short (A.U.)', 'Amplitude long (A.U.)', 'R_1^{short} (1/s)', 'R_1^{long} (1/s)', 'Offset (A.U.)']
        self.initial_guess = [1,                        1,                       100,                 10,                  1]
        self.boundaries =   ((0,                        0,                       0,                   0,                   0),
                             (np.inf,                   1.0,                     np.inf,              np.inf,              np.inf))

    def make_fit_func(self):
        # removing the decorators for now as these prevent the curve_fit method from finding the parameters
        # @classmethod
        # @SingleProcessingElement.linearized_fitFunc
        def model(t, amp_s, amp_l, r1_s, r1_l, off):
            return amp_s * np.exp(-r1_s * t) + amp_l * np.exp(-r1_l * t) + off

        self.fit_function = model

    def make_initial_guess(self, x, y):
        # x may not be in increasing order
        t0_ind = np.argmin(x)
        tend_ind = np.argmax(x)
        amp0 = y[t0_ind] - y[tend_ind]
        r1_0 = (x[tend_ind] - x[t0_ind])/3
        self.initial_guess = [amp0*0.8, amp0*0.2, r1_0, r1_0/10, y[tend_ind]]
