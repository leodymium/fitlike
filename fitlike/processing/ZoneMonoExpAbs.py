# -*- coding: utf-8 -*-
"""
Created on Fri Mar  5 16:05:41 2021

@author: Lionel Broche
"""

import numpy as np
from fitlike.ProcessingFunction import Zone2Disp, ElementWithFit


class ZoneMonoExpAbs(Zone2Disp, ElementWithFit):

    def __init__(self, meas_db):
        super().__init__(meas_db)
        self.function_name = 'Mono exponential fit of magnitude'
        self.x_label = ['Larmor Frequency (Hz)']
        self.y_label =       ['Amplitude (A.U)',   'R_1 (s^{-1})',  'offset (A.U.)']
        self.initial_guess = [-10,                   5,              10]
        self.boundaries =   ((-np.inf,               0,              0),
                             (np.inf,                np.inf,         np.inf))

    def make_fit_func(self):
        # removing the decorators for now as these prevent the curve_fit method from finding the parameters
        # @classmethod
        # @SingleProcessingElement.linearized_fitFunc
        def model(t, amp, r1, off):
            return np.abs(amp * np.exp(-r1 * t) + off)
        self.fit_function = model

    def make_initial_guess(self, x, y):
        y = np.abs(y)
        # x may not be in increasing order
        t0_ind = np.argmin(x)
        tend_ind = np.argmax(x)
        # test if there is a zero-crossing:
        if np.min(y) < y[tend_ind]*0.8:
            amp0 = y[t0_ind] + y[tend_ind]
        else:
            amp0 = y[t0_ind] - y[tend_ind]
        self.initial_guess = [amp0, (x[tend_ind] - x[t0_ind])/3, y[tend_ind]]

    def processing_function(self, x, y, dy, mask, entries):
        y = np.abs(y)
        super().processing_function(x, y, dy, mask, entries)
        return self.best_parameters, self.error
