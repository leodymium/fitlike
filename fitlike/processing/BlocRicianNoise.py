# -*- coding: utf-8 -*-
"""
Created on Fri Mar  5 16:08:04 2021

@author: sambourg, Lionel Broche
"""
import warnings
from scipy.stats import rice
from fitlike.ProcessingFunction import Bloc2Zone, ElementWithFit
import numpy as np
from PyAstronomy import pyasl


class BlocT2RicianNoise(Bloc2Zone, ElementWithFit):
    def __init__(self, meas_db):
        """
        T2 fitting algorithm with Rician noise estimation
        :param meas_db: Dataframe containing the dataset to process
        """
        super().__init__(meas_db)
        self.function_name = 'Average of FID with Rician noise filtering'
        self.x_label = ['Time (s)']
        self.y_label =      ['Amplitude (A.U.)', 'T2 value (s)', 'Offset (A.U.)']
        self.initial_guess = [-10,                  5,              10]
        self.boundaries =   ((0,                    0,              0),
                            (np.inf,                np.inf,         2))

    def make_fit_func(self):
        # removing the decorators for now as these prevent the curve_fit method from finding the parameters
        # @classmethod
        # @SingleProcessingElement.linearized_fitFunc
        def model(t, Amp, T2, N):  # Amp, T2, N):
            t = t - t[:, 0:1]
            S = Amp * np.exp(-t / T2)
            SNR = S / N
            with warnings.catch_warnings():  # Raises warning if SNR>37
                warnings.simplefilter("ignore")
                tmp = rice.mean(SNR) * N
            mask = (SNR < 37)
            np.putmask(S, mask, tmp)  # Remove invalid values
            return S
        self.fit_function = model

    def make_initial_guess(self, x, y):
        n = 1 if x.ndim == 1 else x.shape[0]
        return np.array((300,) * n + (5e-4,) * n + (20,) * n)  # Starting values

