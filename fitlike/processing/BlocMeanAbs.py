#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Feb 26 16:28:47 2021

@author: Lionel Broche, University of Aberdeen, UK
"""

from fitlike.ProcessingFunction import Bloc2Zone
import numpy as np


class BlocMeanAbs(Bloc2Zone):

    def __init__(self, meas_db):
        super().__init__(meas_db)
        self.x_label = ['Time (s)']
        self.y_label = ['Average of magnitude (A.U.)']  # y label of the zone object to create
        self.function_name = 'Average magnitude FID'
        self.process_by_batch = True

    # This is the function that should process the average of the data
    def processing_function(self, x, y, dy, mask, entries):
        y_masked = np.ma.masked_where(mask, np.abs(y))
        dy_masked = np.ma.masked_where(mask, dy)
        z = [np.array(y_masked.mean(axis=1))]
        var = dy_masked**2
        std_err = np.sqrt(var.mean(axis=1)/np.sum(~mask, axis=1))
        dz = [np.array(std_err)]
        return z, dz
