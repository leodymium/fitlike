# -*- coding: utf-8 -*-
"""
Created on Fri Mar 12 09:29:45 2021

@author: sambourg, Lionel Broche
"""

from fitlike.ProcessingFunction import Disp2Exp, ElementWithFit
import numpy as np


class DispDipoleDipole(Disp2Exp, ElementWithFit):

    def __init__(self, meas_db):
        super().__init__(meas_db)
        self.function_name = 'Dipole-Dipole coupling, translation'
        self.x_label = ['Experiment']
        self.y_label =      ['Amplitude (s^{-1})', 'tau (s)']
        self.initial_guess = [10,                   1e-6]
        self.boundaries =   ((0,                    1e-9),
                            (np.inf,                1e-2))

    def make_fit_func(self):
        # removing the decorators for now as these prevent the curve_fit method from finding the parameters
        # @classmethod
        # @SingleProcessingElement.linearized_fitFunc
        def model(f, amp, tau):
            R1 = amp * (tau/(1 + (2*np.pi*f*tau)**2) + 4*tau/(1 + (4*np.pi*f*tau)**2))
            return R1

        self.fit_function = model

    # # TODO: test and improve
    # def estimate_parameters(self, x, y):
    #     y0 = y[np.argmin(x)]
    #     i = np.argmin(abs(y - y0 / 2))
    #     w1 = x[i]
    #     tau = 1 / w1
    #     amp = np.sqrt(y0 * w1 / 5)
    #     self.initial_guess = [amp, tau]
