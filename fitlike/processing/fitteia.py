#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
"""


class fitteia():

    def __init__(self):
        self.id = 0

    def set_model(self):
        # Defines the mathematical model for the fit.

    def set_data(self):
        # defines the data to fit

    def start_fit(self):
        # Sends the request to the fitteia engine

    def collect_results(self):
        # Collects the results and add them into the databank

    def make_json_file(self):
        # generates the json file for the computation