# -*- coding: utf-8 -*-
"""
Created on Fri Mar  5 16:05:41 2021

@author: Lionel Broche
"""

import numpy as np
from fitlike.ProcessingFunction import Zone2Disp, ElementWithFit


class ZoneMonoExp(Zone2Disp, ElementWithFit):

    def __init__(self, meas_db):
        super().__init__(meas_db)
        self.function_name = 'Mono exponential fit'
        self.x_label = ['Larmor Frequency (Hz)']
        self.y_label =       ['Amplitude (A.U)',   'R_1 (s^{-1})',  'offset (A.U.)']
        self.initial_guess = [-10,                   5,              10]
        self.boundaries =   ((-np.inf,               0,              0),
                             (np.inf,                np.inf,         np.inf))

    def make_fit_func(self):
        # removing the decorators for now as these prevent the curve_fit method from finding the parameters
        # @classmethod
        # @SingleProcessingElement.linearized_fitFunc
        def model(t, amp, r1, off):
            return amp * np.exp(-r1 * t) + off
        self.fit_function = model

    def make_initial_guess(self, x, y):
        # x may not be in increasing order
        t0_ind = np.argmin(x)
        tend_ind = np.argmax(x)
        self.initial_guess = [y[t0_ind] - y[tend_ind], (x[tend_ind] - x[t0_ind])/3, y[tend_ind]]
