# -*- coding: utf-8 -*-
"""
Created on Fri Mar 12 10:19:20 2021

@author: sambourg, Lionel Broche
"""

from fitlike.ProcessingFunction import Disp2Exp, ElementWithFit
import numpy as np


class DispDipoleElectron(Disp2Exp, ElementWithFit):

    def __init__(self, meas_db):
        super().__init__(meas_db)
        self.function_name = 'Dipole-Electron coupling'
        self.x_label = ['Experiment']
        self.y_label = ['Amplitude', 'tau (s)', 'beta']
        self.initial_guess = [-10, 5, 10]
        self.boundaries = ((0, 0, 0),
                           (np.inf, np.inf, 2))

    def make_fit_func(self):
        # removing the decorators for now as these prevent the curve_fit method from finding the parameters
        # @classmethod
        # @SingleProcessingElement.linearized_fitFunc
        def model(w0, Amp, tau, beta):
            R1 = Amp ** 2 * (7 * tau / (1 + (660 * w0 * tau) ** (beta)) + 3 * tau / (1 + (w0 * tau) ** (beta)))
            return R1

        self.fit_function = model

    def estimate_parameters(self, x, y):
        y0 = np.mean(y[0:10])
        i = np.argmin(abs(y - y0 / 2))
        w1 = x[i]
        beta = 1
        tau = 1 / w1
        amp = np.sqrt(y0 * w1 / 5)
        self.initial_guess = [amp, tau, beta]
