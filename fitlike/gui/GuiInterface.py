from PyQt6.QtWidgets import (
    QApplication, QWidget, QMainWindow,
    QTableView, QToolBar, QLabel,
    QToolBar, QStatusBar, QFileDialog
)
from PyQt6 import QtCore, QtGui
from PyQt6.QtCore import Qt
from PyQt6.QtGui import QAction, QIcon
# Only needed for access to command line arguments
import sys
import os
from FitLike import FitLike
import pandas as pd
#from https://www.pythonguis.com/tutorials/pyqt6-creating-your-first-window/


class TableModel(QtCore.QAbstractTableModel):

    def __init__(self, data):
        super(TableModel, self).__init__()
        self._data = data

    def data(self, index, role):
        if role == Qt.ItemDataRole.DisplayRole:
            value = self._data.iloc[index.row(), index.column()]
            return str(value)

    def rowCount(self, index):
        return self._data.shape[0]

    def columnCount(self, index):
        return self._data.shape[1]

    def headerData(self, section, orientation, role):
        # section is the index of the column/row.
        if role == Qt.ItemDataRole.DisplayRole:
            if orientation == Qt.Orientation.Horizontal:
                return str(self._data.columns[section])

            if orientation == Qt.Orientation.Vertical:
                return str(self._data.index[section])


class MainWindow(QMainWindow):

    def __init__(self, fl):
        super().__init__()
        data = fl.database_manager.database
        self.setWindowTitle("Data table")
        self.table = QTableView()

        self.model = TableModel(data)
        self.table.setModel(self.model)

        self.setCentralWidget(self.table)

        toolbar = QToolBar("Write/read toolbar")
        self.addToolBar(toolbar)

        button_action = QAction("Your button", self)
        button_action.setStatusTip("This is your button")
        button_action.triggered.connect(self.onMyToolBarButtonClick(fl))
        toolbar.addAction(button_action)

    def onMyToolBarButtonClick(self, fl):
        #fname = QFileDialog.getOpenFileName(self, 'Open sdf file', os.getcwd())
        #if fname[0]:
        #    fl.load_file(fname[0])
        fl.load_file()


# start Fitlike
#fl = FitLike()
#app = QApplication([])
#window = MainWindow(fl)
#window.show()
#app.exec()




#TODO: make a class to manage the interface, with fitlike object as an input.