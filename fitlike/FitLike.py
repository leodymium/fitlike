# /bin/python3

# libraries needed for FitLike:
# Numpy
# Pandas
# Scipy
# matplotlib
# PyAstronomy (for noise estimation in FID signals)
# pyqt6

import os
from fitlike.DbManager import DataManager
from fitlike.DataLoader import LoadDataFile
from processing.BlocMean import BlocMean
from PyQt6.QtWidgets import QApplication, QWidget
import sys
from fitlike import *


"""
Class FitLike
This manages the access to the databases containing the project data.
To add the path to your system list, use:

import sys
sys.path.append(path_to_FitLike)

If on Windows, make sure that you use separate your folders with double backslash. 
"""


class FitLike:
    def __init__(self):
        self.database_manager = DataManager()
        self.data_loader = LoadDataFile([], self.database_manager)
        self.app = QApplication(sys.argv)

    def __call__(self):
        pass

    def add_file(self, file_list=None):
        if file_list is None:
            file_list = []
        self.data_loader.add_to_queue(file_list)

    def load_file(self):
        self.data_loader.load_queue()

    def add_pipeline(self):
        # TODO: pipeline management system
        print('TODO: add a pipeline management system')
        bm = BlocMean()
        bm.input_subset = self.database_manager.database
        bm.trigger()
