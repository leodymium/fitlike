#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
@authors: Lionel Broche, Mohsine Mekhfi
"""

import pandas as pd
import matplotlib.pyplot as plt
import numpy as np


class DataVisualisation:

    def __init__(self, db_man):
        self.database_manager = db_man
        self.label_size = 20
        self.title_size = 20
        self.rc_size = 18
        self.figure_size = (7, 6)

    def plot_selection(self, selection: dict, x_style: str, y_style: str):
        # test the inputs
        valid = ['linear', 'log', 'symlog', 'logit', 'function', 'functionlog']
        if x_style not in valid:
            raise ValueError("results: status must be one of %r." % valid)
        if y_style not in valid:
            raise ValueError("results: status must be one of %r." % valid)
        # collect the data
        selection_df = self.database_manager.database[self.database_manager.subset(selection)]
        x_label = selection_df['x_label'].iloc[0]
        y_label = selection_df['y_label'].iloc[0]
        title = ', '.join(selection.values())
        # make the plot
        plt.figure(figsize=self.figure_size)
        for ind in range(len(selection_df)):
            plt.errorbar(selection_df['x'].iloc[ind],
                         selection_df['y'].iloc[ind],
                         selection_df['dy'].iloc[ind])
        # set the figure parameters
        plt.xlabel(x_label, fontsize=self.label_size)
        plt.ylabel(y_label, fontsize=self.label_size)
        plt.title(title, fontsize=self.title_size)
        plt.xscale(x_style)
        plt.yscale(y_style)
        plt.rc('font', size=self.rc_size)
        plt.grid()
        plt.draw()
        plt.waitforbuttonpress()
