# -*- coding: utf-8 -*-
"""
Created on Thu Feb 11 10:45:16 2021

@author: Lionel Broche
"""

# /bin/python3

from fitlike.devices.LoadStelarFile import LoadSdfV1 as sdfv1
from fitlike.devices.LoadStelarFile import LoadSdfV2 as sdfv2
from fitlike.devices.LoadStelarFile import ReadSef as sef
from fitlike.DbManager import DataManager as dm
import pandas as pd
from PyQt6.QtWidgets import QApplication, QWidget

class LoadDataFile:
    """
    This class contains the methods used to import sdf files version 2.
    The procedure used to import the data consists in storing each acquisition separately in the database as a DataUnit
    entry. To do this, the method loads all the metadata corresponding to the acquisition (protocol parameters) and
    stores a reference to it in each DataUnit entry.
    Procedure: see doc
    """

    def __init__(self, file_list: list, data_manager: dm):
        super().__init__()
        self.data_manager = data_manager
        self.file_queue = file_list  # contains the name of the file to import
        self.reader_version = None  # version of the SDF file reader
        self.metadata_list = []  # acquisition protocols extracted from the file. One AcqProtocol object per headers encountered.
        self.file_reader = None  # reader class to manage the files
        self.current_coordinate = []  # coordinates of the current acquisition in the experimental space
        self.database = data_manager.generate_data_table(0)  # pd.DataFrame(data=None, columns=None)  # database to store all the processed elements
        self.switcher = {  # action taken for each type of key provided by the reader
            'new parameters': self.clear_param,
            'update parameters': self.read_param,
            'update sequence': self.read_sequence,
            'new coordinates': self.read_coordinates,
            'new data': self.read_data,
            'end of file': self.remove_from_queue
        }

    # -------------------------------------------------------------------
    # functions defining the behaviour of the state machine that reads the content of a file:
    # --------------------------------------------------------------------
    def add_to_queue(self, file_list: list = None):
        # TODO: add a method for manual selection using a GUI
        if file_list:
            self.file_queue = self.file_queue + file_list
        else:
            filename = QWidget.QFileDialog.getOpenFileName(self,tr("Open Image"), "/home/jana", tr("Image Files (*.png *.jpg *.bmp)"))
            print('TODO: add a graphical interface to select data files')
            print('For now, put the files with their full path into the field ''file_queue''. It is a list of files.')
            print(filename)

    def remove_from_queue(self):
        # remove the current file from the queue
        self.file_queue.pop(0)
        print('Done')

    def load_queue(self):
        # start processing the queue
        print('Starting the data import')
        while self.file_queue:
            self.process_next_file()
        self.data_manager.append(self.database, self.metadata_list)
        print('Import finished')

    def process_next_file(self):
        # Generate the correct reader for the format (SDFv1 or SDFv2)
        print('Processing file ' + self.file_queue[0] + ' :')
        with open(self.file_queue[0]) as file_handle:  # files are opened here and closed when this function exits
            self.generate_file_reader(
                file_handle)  # this finds the version of the sdf file and generate the appropriate reader
            if self.file_reader is None:
                print('File version not found.')
            else:
                print('File format: ' + self.file_reader.file_format)
                while self.file_reader.current_state != 'end of file':
                    if self.file_reader.line_content is None:
                        self.file_reader.find_next_tag()
                    action = self.switcher[self.file_reader.current_state]
                    action()  # runs the corresponding action
                self.save_data()
        self.remove_from_queue()

    def clear_param(self):
        # store the previous parameters and initialise a new set
        self.file_reader.new_parameter_header()
        self.read_param()

    def read_param(self):
        # Read the content of the parameter header. It is stored in the reader object field 'parameter_header'.
        self.file_reader.read_param_header()

    def read_sequence(self):
        # reads the sequence name if it is separate from the rest of the parameter header
        self.file_reader.read_sequence_name()

    def read_coordinates(self):
        # find the experimental coordinates of the acquisition
        self.file_reader.read_data_coordinates()

    def read_data(self):
        # Read the content of the data section, including the header. The result is stored in the reader object.
        self.file_reader.read_data()

    def save_data(self):
        # save the data collected
        self.metadata_list.extend(self.file_reader.parameter_list)
        self.database = pd.concat([self.database,self.file_reader.data_content])


    # --------------------------------------------------------------
    # File detection functions
    # --------------------------------------------------------------
    def generate_file_reader(self, file_handle):
        # this function finds keywords that are specific to certain file formats to choose the reader.
        format_dict = {
            'USER=':                sdfv1,
            'VERSION = 2':          sdfv2,
            'NMRD SEQUENCE NAME':   sdfv2,
            '_BRLX______':          sef}
        key = ''
        for line_content in file_handle:  # check each line until we find a clue
            entry_check = [line_content.startswith(key) for key in
                           format_dict]  # compare the line with the dictionary entries (this is tricky because we need to find the key within the string)
            if True in entry_check:  # avoid problems due to errors with the index method at the line below
                key = list(format_dict)[entry_check.index(True)]  # find which key has been found
                break
        if not key:
            self.file_reader = None
        else:
            self.file_reader = format_dict.get(
                key)()  # generate the loader file (i.e. load_sdf_v1('file name'), and so on)
            self.file_reader.file_handle = file_handle  # pass the file handle to the file reader
            self.file_reader.file_handle.seek(0)  # position the cursor to the start of the file
