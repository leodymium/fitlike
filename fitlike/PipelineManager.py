#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sun Mar 28 18:43:31 2021

@author: Lionel Broche, University of Aberdeen, UK

pipelines are defined as dataframes containing processing objects. This is
to allow processing different branches and keeping track of the input and
outputs of each branch.

Example of data processing pipeline:

measurement ---- Select -- process       ----- process    ---- extract
dataframe    -   NP/S      Magnetisation   -   dispersion      fit 
             -             decay           -   profiles        parameters
             -                             -
             --- Select -- process      ----
                 PP/S      Magnetisation
                           decay

Branch 1 : selection of NP/S data and processing of their magnetisation decay
            curves.
Branch 2 : selection of PP/S data and processing of their magnetisation decay
            curves. This can be done in parallel to the first segment, so it 
            is at the same level as Branch 1.
Branch 3 : merging the outputs of branches 1 and 2 and processing the 
            dispersion profiles and fit parameters
            
Pipeline: dataframe with branch 1 and 2 both using the same input
                         branch 3 using the merged outputs of the other two 
                         
Each branch has an ID to keep track of inputs and outputs. Branch ID 0 refers
to the initial dataset (meas_db). A status 

Pipeline Dataframe content:
pipeline ID    List of processes       branch ID       input branch ID   status
                     
The pipeline is initialised by populating it with lists of SingleProcessingElement
elements for each branch. The class takes it in charge to pass the output of 
one process to the intput of the next one. The entire process could be written
in pseudo-code as follows:


algorithm:
    start the pipeline
        set all branches statuses to 'unprocessed'
        find branches that need input ID = 0, provide them with the entire
            measurement dataset and start their process chain.
        repeat
            make a list of the branches with status = processed
            check for branches with (status = unprocessed or modified) AND (input branch aklready processed)
            If there are any
                process them
            else
                terminate the repeat loop
        if all branches were processed
            finish
        else
            warning about non processed branches
            finish
            

"""

# TODO multithreading capabilities
# import threading
import ProcessingFunction as pf
import pandas as pd
from fitlike.DbManager import DataManager as dbm
from collections import ChainMap  # chainmaps are collections of dictionaries that are searched together


# A branch is a list of processes that are processed in series.
class Branch():

    def __init__(self):
        self.id = 0
        self.process_list = list

    def add_process(self, process = pf.SingleProcessingElement):
        self.process_list = self.process_list.append(process)
        # TODO: make sure that the output of the previous function goes to the input of the next one
    def remove_process(self, process_index):
        self.process_list = self.process_list.remove(process_index)
        #TODO: same as above

    def replace_process(self, process_index, new_process):
        self.process_list{process_index} = new_process
        # TODO: same as above

    def check_consistency(self):
        # check that the order of the processing function fits the type of inputs and outputs
        pass

# Nodes are elements that aggregate dataframes from upstream branches and make it available for downstream branches, or
# split the output of a branch into several other branches. Not sure if needed, this may be done directly by using
# the selection rules from the processing function.
class Node:
    def __init__(self, selection_rules: dict, branch_list: Branch):
        self.id = ''
        self.selection_rules = selection_rules  # dictionary: {branch_id: selection_dict}
        self.branch_list = branch_list

    def connect_input(self):
        pass
    #todo: make a procedure to merge outputs into a single frame.

    def connect_output(self):
        pass
    #todo: set the rules to distribute multiple outputs to branches.

    def distribute_data(self, incoming_data):
        for branch_id in self.selection_rules.keys():
            branch = self.branch_list[self.branch_list.apply(id) == branch_id]


def list_of_available_process(input_type, output_type):
    """list_of_available_process  Provide the list of processes available
    together with their intended inputs/outputs It is intended to be used
    to create the database of pipelines.
    list_of_available_process(input_type,output_type)
    where input and output types are 'Bloc', 'Zone', 'Disp' or 'Exp'.
    """
    proc_type = input_type + '2' + output_type
    lst = os.listdir(Path('./fitlike/controller/processing/' + proc_type))
    lst = [name for name in lst
           if ~os.path.isdir(name) and name[0] != '_']
    return lst

# A pipeline object is a collection of nodes and branches that provides a complete data processing action
class Pipeline:
    def __init__(self,database_manager: dbm, selection_dictionary: dict):
        self.database_manager =  database_manager                       # reference to the main dataset manager
        self.selection_dictionary = selection_dictionary                # dictionary used to select the data subset used as an input
        self.coordinates = {}                                           # experimental coordinates selected to perform the projection
        self.process_list = pd.dataframe(data = None, 
                                         columns = ['branch_id',
                                                    'process_list',
                                                    'input_node',
                                                    'output_node',
                                                    'status'])
        self.node_list = pd.dataframe(data = None, 
                                         columns = ['node_id',
                                                    'dataset'])

    def data_selector(self, selector_rule: dict):
        # defines the selection selection_rules for the pipeline input. This is defined by a dictionary:
        # selector_rule = {'col_1': [list of elements allowed], col2: [other list of elements allowed], ...}

    # TODO: many tools are needed to help building pipelines and checking their integrity.
    def check_pipeline(self):
        pass # TODO
        
    def summarise_pipeline(self):
        pass
        
    def make_branch(self):
        self.process_list
    
    def add_process_to_branch(self,branchID,process):
        pass
    
    def connect_branch_to_node(self,upstream_node,downstream_node):
        pass
    
    # set all the processes to 'unprocessed' in the pipeline with provided ID
    def initialise_processes(self,pipeline_number):
        self.process_list['status'] = 'unprocessed'
        pass #TODO
    
    def process_branch(self,branch):
        branch['status'] = 'processed' # TODO
        return branch 

    def launch_pipeline(self,pipeline_id):
        processed_branches = [0]
        pipeline = self.process_list[self.process_list['pipeline_id']==pipeline_id] # select the entire pipeline
        while 1:
            branches_to_process = pipeline[(pipeline['status']=='unprocessed') &
                                           (pipeline['input_branch_id'] in processed_branches)]
            if branches_to_process.size[0] == 0:
                break
            for branch in branches_to_process:
                branch = self.process_branch(branch)
                processed_branches.append(branch['branch_id'])
        branches_left = pipeline[pipeline['status']=='unprocessed']
        if branches_left.size[0]>0:
            print('warning:some branches have not been processed')
            
