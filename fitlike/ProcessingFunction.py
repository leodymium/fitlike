#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Feb  5 16:31:11 2021

@author: Lionel Broche, University of Aberdeen, UK
this class defines the structure and general attribute of all the objects
that are used to process the data units. It aims at facilitating
operations on the processing functions, to streamline the creation of
new models and to make it easier to make new models for beginners.
"""

from abc import abstractmethod, ABC
import numpy as np
import os
from pathlib import Path
import pandas as pd
from scipy.optimize import curve_fit


def generate_empty_data_table(n_entries, column_names=None):
    if column_names is None:
        column_names = dict()
    return pd.DataFrame(columns=column_names.keys(), index=range(n_entries))


class SingleProcessingElement:

    def __init__(self, meas_db):

        # these should be abstract
        self.x_label = ''  # describes the new x-axis label to assign (multiple entries for function providing multiple values)
        self.y_label = ['']  # describes the output of the process function (multiple entries for function providing multiple values)
        self.projection_coord = ''  # name of the database column along which we perform the projection
        self.selection_coord = ''  # name of the columns used to single out each new data point TODO: change this and the one above for a dictionary
        self.selection_rule = '' # TODO: replaces the two lines above by using a dictionnary (coord=rule)
        self.function_name = ''  # name of the model, as appearing in the figure legend
        self.input_type = ''  # Name of the input object class (zone, bloc, etc...)
        self.output_type = ''  # name of the output object class
        self.legend = ''  # cell of strings, contain the legend associated with the data processed (name of the contributions, in case of multiple outputs)
        self.input_subset = meas_db  # list of input DataUnits to be processed (auto-populated using the field 'selection_rule')
        self.output_subset = None  # list of output objects that have been processed
        self.todate_flag = True  # update flag set to false when the output is to be re-processed
        self.process_by_line = True  # applies the function line by line or send entire data table

    @abstractmethod
    def processing_function(self, x, y, dy, mask, database_entries):
        """processing_function defines the actual function that performs the 
        data processing operations, i.e. curve 
        fitting, averaging, filtering, etc. It should be a simple method 
        defined by custom-made children classes. 
        This function is called when the processing function is triggered via 
        trigger_processing.
        Outputs: z and dz are lists of numpy arrays, each element of the list correspond to a result from the algorithm.
        Tto be used in the downstream processing object."""
        z = [[]]
        dz = [[]]
        return z, dz

    # process a single array of [x,y,dy,mask]
    def process_single_line(self, line):
        z, dz = self.processing_function(line['x'], line['y'], line['dy'], line['mask'],
                                         line)  # this is a bit ugly but boolean arrays don't concatenate well. Ideally we should be using masked arrays from numpy.
        return [z, dz]

    # Process by batch if data with identical size. create one sub-component per category of sizes.
    def process_by_data_size(self):
        new_entries = pd.DataFrame(columns=self.input_subset.columns)
        size_list = self.input_subset['y'].apply(lambda s: s.size)
        for sze in size_list.unique():
            selection = size_list == sze
            x = np.vstack(
                self.input_subset['x'][selection])  # need to unwrap to avoid issues with DataFrames conversion
            y = np.vstack(self.input_subset['y'][selection])
            dy = np.vstack(self.input_subset['dy'][selection])
            mask = np.vstack(self.input_subset['mask'][selection])
            z, dz = self.processing_function(x, y, dy, mask, self.input_subset[selection])
            if new_entries is None:
                new_entries = self.generate_output(z, dz, self.input_subset[selection])
            else:
                new_entries = new_entries._append(self.generate_output(z, dz, self.input_subset[selection]))
        return new_entries

    # dispatches the outputs of the processing function to the data units downstream
    def generate_output(self, z, dz, input_entries):
        """ generate_output regroups the output values from the processing
        functions and place them into new data entries. The way that is used
        to group the data depends on the type of data objects, i.e. zone 
        objects are formed by selecting blocs with the same zone ID, disp ID, 
        exp ID and file name, whereas disp object do no need to consider the 
        zone ID.
        The data objects created by this function are stored in the meas_db
        database of the calling class."""
        # check if the new object needs to generate a new axis
        if self.input_type == self.output_type:
            new_entries = input_entries.copy()
            new_entries['y'] = z
            new_entries['dy'] = dz
        else:
            new_entries = self.format_output(z, dz, input_entries)
        return new_entries

    def format_output(self, z, dz, input_entries):
        # initialise the output dataframe:
        out = pd.DataFrame(columns=input_entries.columns)
        # Find all the experimental coordinates:
        # indexes are the various groups, last column indicates the number of entries to find
        groups = input_entries[self.selection_coord].groupby(self.selection_coord, sort=False).size()
        # now find the points to project at each coordinate group:
        for gp in groups.index:
            sd = {e: g for e, g in zip(self.selection_coord, gp)}  # selection dictionary
            group_selection = np.all(input_entries[list(self.selection_coord)] == pd.Series(sd), axis=1)
            # now make the new entry for each out the algorithm output:
            projection = input_entries[group_selection]
            if not self.projection_coord:
                x_axis = np.array(range(group_selection.shape[0]))
            else:
                x_axis = projection[self.projection_coord].to_numpy()
            for component_name, z_comp, dz_comp in zip(self.y_label, z, dz):
                new_entry = projection.iloc[0].copy().to_dict()  # columns that are not updated are retained
                new_entry.update({'y': z_comp[group_selection],
                                  'dy': dz_comp[group_selection],
                                  'mask': np.array([False for n in range(z_comp[group_selection].size)]),
                                  'x': x_axis,
                                  'type': self.output_type,
                                  'x_label': self.x_label[0],
                                  'y_label': component_name,
                                  'up_to_date': True})
                out.loc[out.shape[0]] = new_entry
        return out

    def trigger(self):
        """
        trigger_processing launches the data processing function associated
        with the object and append the new child data to the measurements'
        database returned.

        Returns
        -------
        Pandas dataframe with the child objects append.

        """
        # apply the function to the data (this gives a two-columns dataframe with z and dz)
        # initialise an empty results table if this is the first time the input data is processed or if the previous results must be flushed
        new_entries = None
        if not self.input_subset.empty:
            if self.process_by_line:
                # process by concatenating the inputs. This requires to select datasets that have the same length.
                new_entries = self.process_by_data_size()
            else:
                # processing line-by-line:
                out_db = self.input_subset.apply(self.process_single_line, axis=1)
                out_db = out_db.to_numpy()
                z = []
                dz = []
                for comp in range(out_db[0][0].__len__()):
                    z.append(np.array([out_db[n][0][comp] for n in range(out_db.shape[0])], dtype=object))
                    dz.append(np.array([out_db[n][1][comp] for n in range(out_db.shape[0])], dtype=object))
                new_entries = self.generate_output(z, dz, self.input_subset)
        # Saving the results. TODO: use the flush flag to decide whether to erase or update the output
        # if self.flush_flag:
        #     self.output_subset = generate_empty_data_table(0)
        #     self.flush_flag = False
        self.output_subset = new_entries
        return self.output_subset

    # noinspection PyPep8Naming
    def linearized_fitFunc(fitFunc):
        # Decorator function
        # This decorator allows batch processing of fitFunc with curve_fit 
        # it simply organises the parameters sent to curve_fit so that it
        # makes sense when it gets to fitFunc.
        def wrapper(*args):
            # args[0] = self, args[1] = x, args[2:] = fitFunc parameters
            x = args[1]

            n = 1 if x.ndim == 1 else x.shape[0]  # Number of curves in x
            m = int((len(args) - 2) / n)  # Number of parameters in fitFunc

            args_in = []  # Classification of input arguments for fitFunc
            for i in range(m):
                i0 = i * n + 2
                args_in.append(np.expand_dims(np.array(args[i0:i0 + n]), 1))

            return fitFunc(x, *args_in).reshape(-1)

        return wrapper

    # The following line makes the decorator accessible from child classes
    linearized_fitFunc = staticmethod(linearized_fitFunc)


# -------------------------------------------------------------------------------------------------------------
# Classes derived from SingleProcessingElement
# -------------------------------------------------------------------------------------------------------------

# Same as above, but using fitting methods for processing
class ElementWithFit(SingleProcessingElement, ABC):
    """
    ElementWithFit(selection_dataframe)
    creates a process function associated with the data contained in the selection.
    This class contains the tools required to perform curve fitting.
    """

    def __init__(self, meas_db):
        super().__init__(meas_db)
        super().__init__(meas_db)
        self.function_name = ''  # Appears in the figures legends
        self.initial_guess = []  # List of floats
        self.boundaries = ((), ())  # Tuple used by scipy.curve_fit as boundaries. First set are min, second max
        self.fit_function = None  # function handle used by the fit algorithm
        self.algo_parameters = dict()  # list of parameters provided to the fit algorithm, if needed.
        self.best_parameters = None  # best set of parameters found by the fit
        self.covariance_mat = None  # covariance matrix as provided by curve_fit
        self.error = None  # standard deviation estimates of the best parameters
        self.make_fit_func()

    def make_initial_guess(self, x, y):
        pass

    def make_fit_func(self):
        # returns a function for the fit
        return

    def processing_function(self, x, y, dy, mask, entries):
        mask = np.logical_or(mask, np.isnan([*y]))  # mask NaN and Inf values
        self.make_initial_guess(x[~mask], y[~mask])
        try:
            self.best_parameters, self.covariance_mat = curve_fit(self.fit_function,
                                                                  x[~mask],
                                                                  y[~mask],
                                                                  p0=self.initial_guess,
                                                                  bounds=self.boundaries,
                                                                  **self.algo_parameters)
            self.error = np.sqrt(np.diag(self.covariance_mat))
        except RuntimeError:
            self.best_parameters = np.ones([1, self.initial_guess.__len__()])[0] * np.nan
            self.covariance_mat = None
            self.error = np.ones([1, self.initial_guess.__len__()])[0] * np.nan
        return self.best_parameters, self.error


# TODO: create a class to handle fits that are sums of several models.
class ElementWithFitComponents(ElementWithFit):

    def __init__(self):
        pass


# --------------------------------------------------------------------------------------------------------------
# definition of the types of processing functions
# --------------------------------------------------------------------------------------------------------------


class Bloc2Bloc(SingleProcessingElement, ABC):

    def __init__(self, meas_db):
        super().__init__(meas_db)  # Make sure this is the last line of the init section
        self.input_type = 'bloc'
        self.output_type = 'bloc'
        self.projection_coord = None
        self.x_axis = 'Evolution time (s)'
        self.selection_coord = ['file', 'rec', 'b_evo', 'b_ind', 'sequence', 't_evo', 'y_label']


class Bloc2Zone(SingleProcessingElement, ABC):

    def __init__(self, meas_db):
        super().__init__(meas_db)  # Make sure this is the last line of the init section
        self.input_type = 'bloc'
        self.output_type = 'zone'
        self.projection_coord = 't_evo'
        self.x_axis = 'Evolution time (s)'
        self.selection_coord = ['file', 'rec', 'b_evo', 'b_ind', 'sequence', 'y_label']


class Zone2Zone(SingleProcessingElement, ABC):

    def __init__(self, meas_db):
        super().__init__(meas_db)  # Make sure this remains the last line of the __init__ section
        self.input_type = 'zone'
        self.output_type = 'zone'
        self.projection_coord = None
        self.x_axis = None
        self.selection_coord = ['file', 'rec', 'b_evo', 'sequence', 'y_label']
        self.process_by_line = False


class Zone2Disp(SingleProcessingElement, ABC):

    def __init__(self, meas_db):
        super().__init__(meas_db)  # Make sure this remains the last line of the __init__ section
        self.input_type = 'zone'
        self.output_type = 'disp'
        self.projection_coord = 'b_evo'
        self.x_axis = 'Evolution field (T)'
        self.selection_coord = ['file', 'rec', 'sequence', 'y_label']
        self.process_by_line = False


class Disp2Disp(SingleProcessingElement, ABC):

    def __init__(self, meas_db):
        super().__init__(meas_db)  # Make sure this remains the last line of the __init__ section
        self.input_type = 'disp'
        self.output_type = 'disp'
        self.projection_coord = None
        self.x_axis = None
        self.selection_coord = ['file', 'rec', 'sequence', 'y_label']
        self.process_by_line = False


class Disp2Exp(SingleProcessingElement, ABC):

    def __init__(self, meas_db):
        super().__init__(meas_db)  # Make sure this remains the last line of the __init__ section
        self.input_type = 'disp'
        self.output_type = 'exp'
        self.projection_coord = None
        self.x_axis = None
        self.selection_coord = ['file', 'y_label']
        self.process_by_line = False


class Exp2Exp(SingleProcessingElement, ABC):

    def __init__(self, meas_db):
        super().__init__(meas_db)  # Make sure this remains the last line of the __init__ section
        self.input_type = 'exp'
        self.output_type = 'exp'
        self.projection_coord = None
        self.x_axis = None
        self.selection_coord = ['y_label']
        self.process_by_line = False
