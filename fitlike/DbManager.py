#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import pdb  # debug library
import numpy as np
import pandas as pd

"""
This file contains the classes and methods required to handle the databases (data and metadata).
The data is stored in a pandas dataframe, which is accessed by keywords found from the data files.
The metadata is stored in a list. Each element is a dictionary that contains all the parameters found in the files 
imported. Each data entry is linked to a metadata dictionary.
"""


class DataManager:

    def __init__(self):
        self.database = pd.DataFrame(data=None, columns=None)
        self.metadata = []
        self.database = self.generate_data_table(0)

    def generate_data_table(self, n_entries):
        # links the columns to their corresponding types
        if self.database.shape[0] > 0:  # add the columns already present in the data
            type_dict = dict()
            for col_name in self.database.columns:
                type_dict.update({col_name: self.database[col_name].dtype})
        else:
            type_dict = {'type': str,  # type of entry (bloc, zone, disp or exp)
                         'file': str,  # file name, including path
                         'rec': int,  # index of the acquisition within the file
                         'x': object,  # data along the x-axis
                         'x_label': str,  # type of x-data (name of the x-axis)
                         'y': object,  # data along the y-axis
                         'y_label': str,  # type of y-data (name of the y-axis)
                         'dy': object,  # error on y
                         'mask': object,  # masking array (0 if unmasked)
                         'up_to_date': bool,  # flag to check if the data entry needs to be re-processed
                         'metadata': object}  # link to the metadata (parameters linked to the acquisition)
        # dataframes cannot pass more than one dtype when created. Forcing the type is done after creation.
        df = pd.DataFrame(columns=type_dict.keys(),
                          index=range(n_entries))
        for key, val in type_dict.items():
            df[key] = df[key].astype(val)
        return df

    def append(self, datatable, metadata=None):
        # append a new datatable to the database
        self.database = pd.concat([self.database,datatable], ignore_index=True)
        if metadata is not None:
            self.metadata.append(metadata)
        # self.database = self.database.set_index(['file', 'rec'])  # make sure that the file indexing refers to unique acquisitions

    def subset(self, selection_dictionary):
        # generate a subset from selection selection_rules. Can have multiple entries for one dictionary key
        selection = pd.Series(data=True, index=self.database.index, dtype=bool)
        for parameter_name in selection_dictionary.keys():
            values = selection_dictionary[parameter_name]
            if type(values) == list:
                selection = selection & (self.database[parameter_name].isin(values))
            else:
                selection = selection & (self.database[parameter_name] == values)
        return selection

    def subset_by_pattern(self, selection_dictionary):
        # generate a subset from a pattern contained in the entries. Only one entry per check.
        selection = pd.Series(data=True, index=self.database.index, dtype=bool)
        for parameter_name in selection_dictionary.keys():
            selection = selection & (self.database[parameter_name].astype('str').apply(
                lambda s: selection_dictionary[parameter_name] in s) == True)
        return selection

    def data_length(self, selection=None):
        # returns the length of each data recording, using the 'x' column
        if selection is None:
            size_series = self.database['x'].apply(lambda s: s.size)
        else:
            size_series = pd.Series(index=self.database.index)
            size_series[selection] = self.database[selection]['x'].apply(lambda s: s.size)
        return size_series

    def subset_by_data_length(self, length=12, selection=None):
        # provides a selection Series containing the length of each array x, y, dy
        if selection is None:
            subset = self.database.apply(lambda s: s.size == length)
        else:
            subset = pd.Series(index=self.database.index, data=False)
            subset[selection] = self.database[selection]['x'].apply(lambda s: s.size == length)
        return subset

    def list_of_sequences(self):
        return list(self.database['sequence'].unique())

    def subset_from_metadata(self):
        # TODO: provide a selection series from a parameter in the metadata
        pass
