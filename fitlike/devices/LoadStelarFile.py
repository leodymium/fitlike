# /bin/python3
# -*- coding: utf-8 -*-

"""
@author: Lionel Broche, University of Aberdeen, 10/01/2022

This file contains all the import classes for the Stelar files formats, for all versions.
The main class is ReadSdf, from which the others are derived. It contains abstract methods and properties.
"""

from abc import ABC, abstractmethod
from PyAstronomy import pyasl
import numpy as np
import csv
import pandas as pd
from copy import copy


class ReadSdf(ABC):

    def __init__(self):
        self.parameter_list = []  # list of parameters dictionaries
        self.data_content = pd.DataFrame(data=None, columns=['t_evo',
                                                             't_ind',
                                                             'b_evo',
                                                             'b_ind',
                                                             'metadata',
                                                             'x',
                                                             'y',
                                                             'dy',
                                                             'mask'])  # all data read NOTE: used to be  set for the columns
        self.data_coord = None  # data-specific header, if any
        self.n_bloc = 1  # number of blocs (default to 1)
        self.file_handle = None  # handle of the current file
        self.line_content = None  # content of the line being read
        self.current_state = None  # latest tag found
        self.tag_selector = dict()  # dictionary describing the types of tags in the file. Version-dependant. (TODO: must be an abstract property)
        self.file_format = None  # name of the format used, for information
        self.csv_reader = None
        self.record_number = -1  # index of the experiment within the file, to keep track of multiple recordings in the same file
        self.beq = pyasl.BSEqSamp()

    def new_parameter_header(self):
        self.parameter_list.append(dict())
        self.record_number += 1

    # find the gyromagnetic ratio in Hz/T
    def gamma(self):
        select_gyromag = {
            '1H': 42.5775e6,
            '2H': 6.536e6,
            '3H': 45.415e6,
            '3He': -32.434e6,
            '6Li': 6258892.5,
            '7Li': 16.546e6,
            '9Be': 6003427.5,
            '10B': 4555792.5,
            '11B': 13667377.5,
            '14Na': 3065580.0,
            '13C': 10.7084e6,
            '14N': 3.077e6,
            '15N': -4.316e6,
            '17O': -5.772e6,
            '19F': 40.078e6,
            '23Na': 11.262e6,
            '25Mg': 2597227.5,
            '27Al': 11.103e6,
            '29Si': -8.465e6,
            '31P': 17.235e6,
            '33S': 3278467.5,
            '35Cl': 4172595,
            '37Cl': 3491355,
            '39K': 2001142.5,
            '43Ca': 2852692.5,
            '45Sc': 10346332.5,
            '47Ti': 2384340,
            '49Ti': 2384339,
            '57Fe': 1.382e6,
            '51V': 11197882.5,
            '53Cr': 2426917.5,
            '55Mn': 10516642.5,
            '77Se': 8132302.5,
            '89Y': 2086297.5,
            '103Rh': 1362480.0,
            '107Ag': 1703100.0,
            '109Ag': 2001142.5,
            '111Cd': 9026430.0,
            '113Cd': 9452205.0,
            '117Sn': 15157590.0,
            '119Sn': 15881407.5,
            '125Te': 13411912.5,
            '_129Xe': 11836545.0,
            '169Tm': 3533932.5,
            '171Yb': 7493640,
            '183W': 1788255.0,
            '187Os': 979282.5,
            '195Pt': 9111585,
            '199Hg': 7621372.5,
            '203Ti': 24311752.5,
            '205Ti': 24524640.0,
            '207Pb': 8898697
        }
        if 'nucleus' in self.parameter_list[-1]:
            gamma = select_gyromag[self.parameter_list[-1]['nucleus']]
        else:
            gamma = select_gyromag['1H']
        return gamma

    def estimate_noise(self, y):
        # estimate the noise (from https://pyastronomy.readthedocs.io/en/latest/pyaslDoc/aslDoc/estimateSNR.html)
        # Define order of approximation (use larger values such as 2,3, or 4 for
        # faster varying or less well sampled data sets; also 0 is a valid order)
        n = 1
        # Define 'jump parameter' (use larger values such as 2,3, or 4 if correlation
        # between adjacent data point is suspected)
        j = 1
        # Estimate noise assuming equidistant sampling (often a good approximation even
        # if data are not strictly equidistant) and robust estimation (often advantageous
        # in working with real data)
        n_std, n_std_std = self.beq.betaSigma(np.real(y), n, j, returnMAD=True)
        return np.ones(y.shape[0]) * n_std

    def line_has_a_tag(self):
        # find if the current line in the document contains a tag. 0 if not, 1 if yes. Updates the current_tag accordingly.
        out = 0
        entry_check = [self.line_content.startswith(key) for key in
                       self.tag_selector.keys()]  # compare the line with the dictionary entries (this is tricky because we need to find the key within the string)
        if True in entry_check:  # avoid problems due to errors with the index method at the line below
            key_found = list(self.tag_selector)[entry_check.index(True)]  # find which key has been found
            self.current_state = self.tag_selector[key_found]
            out = 1
        elif not self.line_content:
            self.current_state = 'end of file'
            out = 1
        return out

    def find_next_tag(self):
        # set the current line to the start of the next parameter header. Returns 1 on new header, 2 on updated header, 3 on new data, 0 on end of file
        self.current_state = 'end of file'  # default value, in case the loop finishes before a tag is found
        for line_content in self.file_handle:  # check each line until we find a clue
            self.line_content = line_content  # update to the latest content
            if self.line_has_a_tag():
                break
        return

    @abstractmethod
    def read_param_header(self) -> dict:
        # reads a section of parameters and returns it as a dictionary
        return dict()

    @abstractmethod
    def read_data(self):
        # read the content of the data section and store it in data_content.
        # the data-specific header is stored in data_coord.
        return NotImplementedError


class LoadSdfV1(ReadSdf):
    """
    =============================================================================
    #           # Tau
    #     def Tau(self):
    #         NBLK = int(float(self.protocol_list[self.zone_id].nblk))     # Nombre de blocs
    #         T1MX = float(self.protocol_list[self.zone_id].t1mx)          # Paramètre du calcul de tau
    #         BINI = 4*T1MX                                                # Tau initial
    #         BEND = 0.01*T1MX                                             # Tau final
    #         dataT = np.zeros(NBLK)
    #         for k in range (NBLK):
    #             dataT[k] = BINI*(BEND/BINI)**(float(k)/float(NBLK-1))    # Calcul des tau, supposé LOG
    #         self.data_tau = dataT
    #
    #
    #     def Zone(self,zone_id):                                # On indique l'indice de la zone que l'on souhaite étudier
    #         self.zone_id = int(zone_id)
    #         self.zone_Brelax = float(self.protocol_list[self.zone_id].brlx)
    #         self.Tau()
    #         self.Mean_data()
    =============================================================================
    """

    def __init__(self):
        super().__init__()
        self.tag_selector = {
            "STELAR Data File": 'new parameters',
            "ZONE": 'update parameters',
            "DATA": 'new data'
        }
        self.sequence = dict()
        self.file_format = 'Stelar data file version 1'
        # column_selector defines the content of the columns depeding on patterns from the pulse sequence name
        self.col_names = []  # data columns are a pain with version 1. A dedicated function is written.
        # [ dict(),  # each entry corresponds to the number of columns in the data
        #                       dict(),  # 1 column: should never happen
        #                       {'': ['real', 'imag']},  # case when only 2 columns are present (must be real/imag)
        #                       {'NP/S': ['real', 'imag', 'square'],  # 3 entries
        #                      'PP/S': ['real', 'imag', 'square'],
        #                      'NP ': ['real', 'imag', 'square'],
        #                      'PP ': ['real', 'imag', 'square'],
        #                      'ANGLE.FFC': ['real', 'imag', 'square'],
        #                      'ssf': ['real', 'imag', 'time']},
        #                       {'CPMG': ['time', 'real', 'imag', 'mod'],  # 4 entries
        #                      'NP/S': ['real', 'imag', 'time', 'square'],
        #                      'PP/S': ['real', 'imag', 'time', 'square'],
        #                      'NP ': ['real', 'imag', 'magnitude', 'square'],
        #                      'PP ': ['real', 'imag', 'magnitude', 'square'],
        #                      'ANGLE.FFC': ['real', 'imag', 'magnitude', 'square'],
        #                      'ssf': ['real', 'imag', 'time', 'square']},
        #                       {'CPMG': ['time', 'real', 'imag', 'mod', 'square'],
        #                        'ssf': ['time', 'real', 'imag', 'mod', 'square']}  # 5 entries
        #                       ]

    def read_header(self):
        # read a typical header containing params or coordinates
        header = {'version': 'sdf 1.0'}
        if self.line_content.find('STELAR Data File') < 0:  # the initial 'stelar data file' header has no info
            header['zone'] = self.line_content[6:-1]  # store the zone number before proceeding
            self.col_names = []  # reinitialise the list of columns in case the sequence changes
            for line_content in self.file_handle:
                self.line_content = line_content
                if self.line_has_a_tag():  # stops when the data starts
                    break
                if self.line_content.find('=') < 0:  # every parameter line has a sign =
                    continue
                param_name = line_content[0:line_content.index('=')]
                param_name = param_name.replace(" ", "")  # there may be some spaces within the parameter names
                param_value = line_content[line_content.index('=') + 2:-1]  # avoid the final carriage return
                if isinstance(param_name, str):
                    header[param_name.lower()] = param_value
                else:
                    breakpoint()
                    print('Error while importing parameter ', param_name, ' with value ', param_value)
        else:
            self.find_next_tag()  # skip the initial line
        return header

    def read_param_header(self):
        # read the content of the sequence parameters header and store it as a parameter header.
        self.parameter_list[-1].update(self.read_header())
        return self.parameter_list[-1]

    # This function provides the list of evolution times (in sec) for a given zone number.
    def t_evo(self):
        # make sure we have what we need
        if ('t1mx' not in self.parameter_list[-1]) or ('tau' not in self.parameter_list[-1]):
            return None
        # get the list of T1MAX values. T1max is defined in ms (it is used in the expressions of the start and end values)
        t1mx_str = self.parameter_list[-1]['t1mx'].lower()
        t1mx = np.array(t1mx_str, 'float')  # needed for the 'eval' statements below. Ugly but necessary.
        # Get the generator parameters ([spacing:start value:end value:#pts])
        method = self.parameter_list[-1]['bgrd']
        te = []
        if method == 'LIST':
            t_lst = self.parameter_list[-1]['blst'].split(';')
            [t_start, t_end] = t_lst[0].split(':')
            [method, n_pts] = t_lst[1].split(':')
            t_start = eval(t_start.lower())
            t_end = eval(t_end.lower())
            n_pts = int(float(n_pts))
        else:
            t_start = eval(self.parameter_list[-1]['bini'].lower())
            t_end = eval(self.parameter_list[-1]['bend'].lower())
            n_pts = int(float(self.parameter_list[-1]['nblk']))
        if method == 'LIN':
            te = np.linspace(t_start, t_end, n_pts)
        elif method == 'LOG':
            te = np.logspace(t_start, t_end, n_pts)
        else:
            print('Error: method unknown')
        return te

    def find_column_names(self, bloc):
        # find the names for each column in the dataset according to the data in a bloc:
        self.col_names = ['col' + str(i) for i in range(bloc.shape[0])]  # default names : col#
        col_ind = list(range(len(self.col_names)))
        # find the time axis by checking for monotonously increasing values:
        dt = np.std(np.diff(bloc, axis=1), axis=1) < 1e-10  # there may be tiny calculation errors, so it may not be 0
        if dt.any():
            ind = np.where(dt == True)[0].tolist()[0]
            int_t = col_ind.pop(ind)
            self.col_names[int_t] = 'time'
        # by default, the real and imaginary data are the first two columns remaining:
        self.col_names[col_ind[0]] = 'real'
        self.col_names[col_ind[1]] = 'imag'
        # we save the rest with the default column values, except if we find a magnitude data:
        if len(col_ind) > 2:
            res = bloc[col_ind[2]] - np.sqrt(bloc[col_ind[0]]**2 + bloc[col_ind[1]]**2)
            if np.std(res)/np.mean(bloc[col_ind[2]]) < 0.1:  # there may be up to 10% error in the magnitude due to rounding errors
                self.col_names[col_ind[2]] = 'magnitude'
        return

    def read_data_coordinates(self):
        if self.parameter_list[-1].keys().__contains__('nblk'):
            self.n_bloc = int(float(self.parameter_list[-1]['nblk']))
            if not self.n_bloc:
                self.n_bloc = 1  # some early stelar file may set NBLK to 0...
        coord = {'t_ind': [range(self.n_bloc) for n in range(self.n_bloc)],
                 'x_label': ['Time (us)' for n in range(self.n_bloc)],
                 'y_label': ['Signal (A.U.)' for n in range(self.n_bloc)],
                 'file': [self.file_handle.name for n in range(self.n_bloc)],
                 'sequence': [self.parameter_list[-1]['exp'] for n in range(self.n_bloc)],
                 'rec': [self.record_number for n in range(self.n_bloc)],
                 'up_to_date': [True for n in range(self.n_bloc)],
                 'type': ['bloc' for n in range(self.n_bloc)],
                 'gamma': [self.gamma() for n in range(self.n_bloc)],
                 'metadata': [self.parameter_list[-1] for n in range(self.n_bloc)]}
        if self.parameter_list[-1].keys().__contains__('zone'):
            coord['b_ind'] = [int(self.parameter_list[-1]['zone']) for n in range(self.n_bloc)]
        if self.parameter_list[-1].keys().__contains__('brlx'):
            coord['b_evo'] = [float(self.parameter_list[-1]['brlx']) * 1e6 for n in range(self.n_bloc)]
        if self.parameter_list[-1].keys().__contains__('t1mx'):
            coord['t_evo'] = self.t_evo()
        self.data_coord = coord
        return coord

    def read_data(self):
        # read the content of the data section and store it in data_content.
        # the data-specific header is stored in data_coord.
        if self.csv_reader is None:
            self.csv_reader = csv.reader(self.file_handle, delimiter='\t')
        # Now import the relevant metadata
        n_pts = int(float(self.parameter_list[-1]['bs']))
        param_dict = self.read_data_coordinates()
        new_bloc = pd.DataFrame()
        # Read all the data in the zone (stops with the first blank line)
        for n in range(self.n_bloc):
            for m in range(n_pts):
                line = [float(i) for i in next(self.csv_reader)]
                if m == 0:
                    datamatrix = np.zeros(shape=(len(line), n_pts), dtype='float')
                # noinspection PyUnboundLocalVariable
                datamatrix[:, m] = np.array(line)
            # the column names can only be initialised after we know what the data looks like:
            if not self.col_names:
                self.find_column_names(datamatrix)  # column names depend on the sequence version
            cols = self.col_names + list(param_dict.keys())
            # now that we know the name for each column we can initialise the dataframe:
            if new_bloc.columns.shape[0] == 0:
                new_bloc = pd.DataFrame(index=range(self.n_bloc), columns=cols, dtype=object, data=param_dict)
            # now save the bloc as a dataframe:
            for col, c in zip(self.col_names, range(len(self.col_names))):  # can be improved
                new_bloc.loc[n, col] = datamatrix[c, :]
        # Save the data by blocs:
        new_bloc['y'] = new_bloc.pop('real') + new_bloc.pop('imag') * 1j
        new_bloc['dy'] = new_bloc['y'].apply(self.estimate_noise)
        if new_bloc.columns.__contains__('time'):  # some pulse sequences don't save the time axis...
            new_bloc['x'] = new_bloc.pop('time') * 1e-6
        else:
            new_bloc['x'] = new_bloc['y'].apply(lambda s: np.array(range(s.shape[0])))
        new_bloc['mask'] = new_bloc['y'].apply(lambda s: [False for n in range(s.shape[0])])
        # now store the data for this bloc.
        self.data_content = pd.concat([self.data_content, pd.DataFrame(new_bloc)], ignore_index=True)
        self.find_next_tag()
        return


class LoadSdfV2(ReadSdf):
    """
    This class contains the methods used to import sdf files version 2.

    The version 2 of the Stelar files are arranged as follows:

        header 1: created each time a profile wizard acquisition is recorded in the file
            STELAR Data File

            VERSION = 2.0.18-FFC
            OWNER = ..
            OPERATOR = ..
            ACQINI = ..
            ACQEND = ..

        header 2: created each time the pulse sequence changes during the acquisition
            NMRD SEQUENCE NAME: C:/Users/PC_NMR/Desktop/sequenze_field/00_NP-S_unbalanced.ssf

            PARAMETER SUMMARY
            (summary of the parameters, param = value for each line)

            header 3: created at the start of each zone:
                ZONE 1.1
                BR = 10.000000000000002
                T1MAX = 400000.0

                DATA
                REAL		IMG		MOD		TIME(us)
                (data follows here, one point per line, all blocs together)

    each section is repeated until the end of the file.
    Note that the parameters from the experimental protocol are split between the two headers. The section in header 1 is called dispersion protocol, the one in header two is the zone protocol.
    The import function works by creating Bloc objects, which store the raw data, and protocol objects, which store the protocol parameters.
    The Blocs are stored in the field bloc_list, the protocols in protocol_list.
    """

    def __init__(self):
        super().__init__()
        self.tag_selector = {
            "STELAR Data File": 'new parameters',
            "NMRD SEQUENCE NAME:": 'update sequence',
            "PARAMETER SUMMARY": 'update parameters',
            "ZONE": 'new coordinates',
            "DATA": 'new data'
        }
        self.sequence = dict()
        self.file_format = 'Stelar data file version 2'

    def read_header(self):
        # read a typical header containing params or coordinates
        header = {'version': 'sdf 2.0'}
        for line_content in self.file_handle:
            self.line_content = line_content
            if self.line_has_a_tag():
                break
            if self.line_content.find('=') < 0:  # every parameter line has a sign =
                continue
            param_name = line_content[0:line_content.index(' =')]
            param_name = param_name.replace(" ", "")  # there may be some spaces within the parameter names
            param_value = line_content[line_content.index(' =') + 3:-1]  # avoid the final carriage return
            if isinstance(param_name, str):
                header[param_name.lower()] = param_value
            else:
                print('Error while importing parameter ', param_name, ' with value ', param_value)
        return header

    def read_param_header(self):
        # read the content of the sequence parameters header.
        param_header = self.read_header()
        self.parameter_list[-1].update(param_header)
        return param_header

    def read_sequence_name(self):
        # reads the name of the pulse sequence and returns it as a parameter dictionary.
        # Keeps the notation convention from sdf version 1. This is changed later.
        self.sequence = self.line_content[20:-1]
        self.find_next_tag()
        return dict()  # returning an empty dictionary to solve issues with the sequence name preceding other params

    # This function provides the list of evolution times (in sec) for a given zone number.
    def t_evo(self):
        # make sure we have what we need
        if ('t1max' not in self.parameter_list[-1]) or ('tau' not in self.parameter_list[-1]):
            return None
        # get the list of T1MAX values. T1max is defined in ms (it is used in the expressions of the start and end values)
        t1max_str = self.parameter_list[-1]['t1max'].lower()
        t1max = np.array(t1max_str, 'float') * 1e-6  # needed for the 'eval' statements below. Ugly but necessary.
        # Get the generator parameters ([spacing:start value:end value:#pts])
        lst = self.parameter_list[-1]['tau'][1:-1].split(':')
        scale = lst[0]  # log or lin scale
        t_start = eval(lst[1].lower())
        tend = eval(lst[2].lower())
        n_pts = int(float(lst[3]))
        # generate the list of evolution times
        te = []
        if scale == 'lin':
            te = np.linspace(t_start, tend, n_pts)
        elif scale == 'log':
            te = np.logspace(np.log10(t_start), np.log10(tend), n_pts)
        else:
            print('Error: incorrect scale type (' + scale + ').')
        return te

    # evolution field in T
    def b_evo(self):
        bevo_str = self.parameter_list[-1]['br'].replace('{', '')
        bevo_str = bevo_str.replace('}', '')
        bevo_str = bevo_str.split(',')
        bevo = np.array(bevo_str, 'float') * 1e6
        return bevo

    def read_data_coordinates(self):
        if self.parameter_list[-1].keys().__contains__('nblk'):
            self.n_bloc = int(float(self.parameter_list[-1]['nblk']))
            if not self.n_bloc:
                self.n_bloc = 1  # some early stelar file may set NBLK to 0...
        # read the section after the ZONE headers, which are data-specific
        parts = self.line_content.split('.')
        coord = self.read_header()
        coord.update({'first_ind': [int(parts[0][5:]) for n in range(self.n_bloc)],
                      'b_ind': [int(parts[1][0:-1]) for n in range(self.n_bloc)],
                      't_ind': [np.array(range(self.n_bloc))],
                      't_evo': [self.t_evo()],
                      'sequence': [self.sequence for n in range(self.n_bloc)],
                      'x_label': ['Time (us)' for n in range(self.n_bloc)],
                      'y_label': ['Signal (A.U.)' for n in range(self.n_bloc)],
                      'file': [self.file_handle.name for n in range(self.n_bloc)],
                      'rec': [self.record_number for n in range(self.n_bloc)],
                      'up_to_date': [True for n in range(self.n_bloc)],
                      'type': ['bloc' for n in range(self.n_bloc)],
                      'gamma': [self.gamma() for n in range(self.n_bloc)],
                      'metadata': [self.parameter_list[-1] for n in range(self.n_bloc)]})
        coord['b_evo'] = coord.pop('br')
        self.data_coord = coord
        return coord

    def read_data(self):
        # read the content of the data section and store it in data_content.
        # the data-specific header is stored in data_coord.
        if self.csv_reader is None:
            self.csv_reader = csv.reader(self.file_handle, delimiter='\t')
        col_names = next(self.csv_reader)[::2]  # loads the description of each column
        n_pts = int(float(self.parameter_list[-1]['bs']))  # have to use int and float to deal with non-integers
        cols = col_names + list(self.data_coord.keys())
        new_bloc = pd.DataFrame(index=range(self.n_bloc), columns=cols, dtype=object, data=self.data_coord)
        # Read all the data in the zone (stops with the first blank line)
        datamatrix = np.ndarray(shape=(len(col_names), n_pts), dtype='float')
        for n in range(self.n_bloc):
            for m in range(n_pts):
                datamatrix[:, m] = [float(i) for i in next(self.csv_reader)[::2]]  # version 2 uses double tabs
            for col, c in zip(col_names, range(len(col_names))):  # can be improved
                new_bloc.loc[n, col] = datamatrix[c, :]
        # Save the data by blocs:
        new_bloc['y'] = new_bloc.pop('REAL') + new_bloc.pop('IMG') * 1j
        new_bloc['dy'] = new_bloc['y'].apply(self.estimate_noise)
        if new_bloc.columns.__contains__('TIME(us)'):  # some pulse sequences don't save the time axis...
            new_bloc['x'] = new_bloc.pop('TIME(us)') * 1e-6
        else:
            new_bloc['x'] = new_bloc['y'].apply(np.array(range(self.n_bloc)))
        new_bloc['mask'] = new_bloc['y'].apply(lambda s: [False for n in range(s.shape[0])])
        # now store the data for this bloc.
        self.data_content = pd.concat([self.data_content,pd.DataFrame(new_bloc)], axis=0, ignore_index=True)
        self.find_next_tag()
        return


class ReadSef(ReadSdf):

    def __init__(self):
        super().__init__()
        self.tag_selector = {
            "STELAR Export File": 'new parameters',
            "_BRLX______": 'new data'
        }
        self.file_format = 'Stelar export file'
        self.parameter_list = []  # list of parameters dictionaries
        self.data_content = pd.DataFrame(data=None, columns=['metadata',
                                                             'x', 'y', 'dy', 'mask'])  # all data read

    def read_header(self):
        # read a typical header containing params or coordinates
        header = {'version': 'sef'}
        for line_content in self.file_handle:
            self.line_content = line_content
            if self.line_has_a_tag():  # stops when the data starts
                break
            if self.line_content.find('=') < 0:  # every parameter line has a sign =
                continue
            param_name = line_content[0:line_content.index('=')]
            param_name = param_name.replace(" ", "")  # there may be some spaces within the parameter names
            param_value = line_content[line_content.index('=') + 2:-1]  # avoid the final carriage return
            if isinstance(param_name, str):
                header[param_name.lower()] = param_value
            else:
                print('Error while importing parameter ', param_name, ' with value ', param_value)
        return header

    def read_param_header(self):
        # read the content of the sequence parameters header and store it as a parameter header.
        self.parameter_list[-1].update(self.read_header())
        return self.parameter_list[-1]

    def read_data(self):
        # read the content of the data section and store it in data_content.
        # the data-specific header is stored in data_coord.
        datamatrix = np.zeros([0, 6], dtype='float')
        self.line_content = self.file_handle.readline()
        while not self.line_has_a_tag():
            if self.line_content == '\n':
                self.line_content = self.file_handle.readline()
                continue
            if not self.line_content:
                break
            # read all the data lines for one bloc
            line_itemised = self.line_content.split()
            datamatrix = np.vstack((datamatrix, ([float(i) for i in line_itemised[0:6]])))
            self.line_content = self.file_handle.readline()
        new_bloc = {'x': [np.transpose(datamatrix[:, 0] * 1e6)],
                    'x_label': ['Field (MHz)'],
                    'y': [np.transpose(datamatrix[:, 2])],
                    'y_label': ['R_1 (s^{-1})'],
                    'dy': [np.transpose(datamatrix[:, 4])],
                    'mask': [np.array([False for n in range(datamatrix.shape[0])])],
                    'file': [self.file_handle.name],
                    'rec': [self.record_number],
                    'type': ['disp'],
                    'gamma': self.gamma(),
                    'up_to_date': [True],
                    'metadata': [self.parameter_list[-1]]}
        self.data_content = pd.concat([self.data_content,pd.DataFrame(new_bloc)], ignore_index=True)
        return
