#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
This script is a first draft for a PP/NP data processing flow using Prefect
"""
import os

from FitLike import FitLike
from processing.BlocMeanAbs import BlocMeanAbs
from processing.BlocMean import BlocMean
from processing.BlocRicianNoise import BlocT2RicianNoise
from processing.ZoneMonoExp import ZoneMonoExp
from processing.ZoneMonoExpAbs import ZoneMonoExpAbs
from processing.ZoneBiExp import ZoneBiExp
from processing.DispDipoleDipole import DispDipoleDipole
from fitlike.DataVisualisation import DataVisualisation
import matplotlib.pyplot as plt

# dirname = os.path.dirname(os.getcwd())
root_dir = os.path.dirname(os.path.abspath(__file__))

# creating the main object. The data files are listed inside by default for debug purposes.
fl = FitLike()

# loading the data
fl.data_loader.add_to_queue([os.path.abspath(root_dir + '/datasets/Datasamplesv2/LS10_001T.sdf'),
                             os.path.abspath(root_dir + '/datasets/Datasamplesv1/sample1 - tumour.sdf'),
                             os.path.abspath(root_dir + '/datasets/Datasamplesv2/LS10 001T.sef')])

fl.data_loader.load_queue()

# selecting all the blocs available, then process the data by blocs according to the sizes of the arrays (we cannot concatenate arrays of different sizes)
all_blocs = fl.database_manager.subset({'type': 'bloc'})
only_ffc = fl.database_manager.subset_by_pattern({'sequence': '/S'})
# prepare the bloc processing functions
bm_list = [BlocMeanAbs(fl.database_manager.database[all_blocs & only_ffc])]
bm_list[0].trigger()
# pass the results to the main object
fl.database_manager.append(bm_list[0].output_subset)

# now we do the same with the zones.
all_zones = fl.database_manager.subset({'type': 'zone'})
pulse_selection = {'00_NP-S_unbalanced.ssf': ZoneMonoExpAbs,
                   '00_PP-S_unbalanced.ssf': ZoneMonoExpAbs,
                   'IRCPMG/S [DefaultFfcSequences.ssf]': ZoneMonoExpAbs,
                   'NP/S [DefaultFfcSequences.ssf]': ZoneMonoExpAbs,
                   'PP/S [DefaultFfcSequences.ssf]': ZoneMonoExp}
zm_list = []
for sel in pulse_selection.keys():
    selection_seq = fl.database_manager.subset_by_pattern({'sequence': sel})
    zm_list.append(ZoneMonoExp(fl.database_manager.database[selection_seq & all_zones]))  # we could put the results from the bloc processing only
    zm_list[-1].trigger()
for zm in zm_list:
    fl.database_manager.append(zm.output_subset)

# and now we do the same with the dispersions.
all_disp = fl.database_manager.subset({'type': 'disp', 'y_label': 'R_1 (s^{-1})'})
dm_list = [DispDipoleDipole(fl.database_manager.database[all_disp])]
dm_list[-1].trigger()
for dm in dm_list:
    fl.database_manager.append(dm.output_subset)

# From here on we can analyse the results
print(fl.database_manager.subset({'type': 'exp'}))
dv = DataVisualisation(fl.database_manager)
dv.plot_selection({'type': 'zone'}, 'linear', 'linear')
dv.plot_selection({'type': 'disp', 'y_label': 'R_1 (s^{-1})'}, 'log', 'log')
