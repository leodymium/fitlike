classdef LorentzianStretched < DispersionModel 
    % Stretched Lorentzian model for liquids with Gaussian diffusion and
    % restricted motion (phenomenological model).
    %
    % Lionel Broche, University of Aberdeen, 08/02/2017 (modified 23/08/18)
    
    properties
        modelName     = 'Streched Lorentzian profile';        
        modelEquation = 'A*[tau./(1+(2*pi*f*tau).^n) + 4*tau./(1+(2*2*pi*f*tau).^n)]';    
        variableName  = {'f'};     
        parameterName = {'A',   'tau',  'n'};  
        minValue      = [0,       0,     0];  
        maxValue      = [Inf,     1    ,   Inf];  
        startPoint    = [10,   1e-6,     1];  
        isFixed       = [0        0         0];
        visualisationFunction cell = {};
    end
    
    methods
        function this = LorentzianStretched
            % call superclass constructor
            this = this@DispersionModel;
        end
    end
    
    methods     
    end
end