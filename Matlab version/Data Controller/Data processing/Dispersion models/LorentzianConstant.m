classdef LorentzianConstant < DispersionModel
    % Lorentzian model for freely-moving molecules with Gaussian diffusion
    % profiles.
    % From: Understanding Spin Dynamics, D. Kruk, Pan Stanford Publishing
    % 2016,  page 20
    %
    % Lionel Broche, University of Aberdeen, 08/02/2017 (modified 23/08/18)
    
    properties
        modelName     = 'LorentzianConstant profile';        
        modelEquation = 'A*[1./(1+(2*pi*f*tau).^2) + 4./(1+(4*pi*f*tau).^2)] + C';    
        variableName  = {'f'};     
        parameterName = {'A',   'tau', 'C'};  
        minValue      = [0,       1e-9, 0];  
        maxValue      = [Inf,     1e-3, 10000];  
        startPoint    = [10,   1e-6, 0.3];  
        isFixed       = [0           0      0];
        visualisationFunction cell = {};
    end
    
     methods
        function this = LorentzianConstant
            % call superclass constructor
            this = this@DispersionModel;
        end
    end
    
    methods
        % function that allows estimating the start point.

    end
end