classdef BiexponentialT1AbsGlobal < Zone2Disp & DataFit
%MONOEXP Compute the 1-exponential decay model. The function is based on a
%non-linear regression using iterative least-squares estimation and returned the
%time constant of the equation y = f(x) with its error as well as the model used.
    properties
        InputChildClass char;
        OutputChildClass char;
        functionName char = 'Biexponential Abs fit (global)';      % character string, name of the model, as appearing in the figure legend
        labelY char = 'R_1 (s^{-1})';                   % string, labels the Y-axis data in graphs
        labelX char = 'Evolution field (MHz)';          % string, labels the X-axis data in graphs
        legendTag cell = {'Long T1','Short T1'};
    end

    properties
       modelName = 'AbsBiexponential T1';          % character string, name of the model, as appearing in the figure legend
       modelEquation = ['sqrt((M0 + A1*exp(-t*R11) + A2*exp(-t*R12))).^2 +'...
                                 'abs(2*(M0 + A1*exp(-t*R11) + A2*exp(-t*R12)).*abs(noise) +'...
                                 '2*noise.^2);'];      % character string, equation that relates the Larmor frequency (Hz) to the parameters to R1 (s^{-1})
       variableName = {'t'};                                  % List of characters, name of the variables appearing in the equation
       parameterName = {'M0','A1','R11','A2','R12','noise'};        % List of characters, name of the parameters appearing in the equation
       isFixed = [0 0 0 0 0 0];                               % List of array of booleans, set to 1 if the corresponding parameter is fixed, 0 if they are to be optimised by the fit.
       minValue = [-Inf -Inf -Inf -Inf 1 -Inf];               % array of values, minimum values reachable for each parameter, respective to the order of parameterName
       maxValue = [Inf Inf 1 Inf Inf Inf];               % array of values, maximum values reachable for each parameter, respective to the order of parameterName
       startPoint = [1 1 1 1 1 1];             % array of values, starting point for each parameter, respective to the order of parameterName
       valueToReturn = [0 0 1 0 1 0];          % set which fit parameters must be returned by the function
       visualisationFunction cell = {};
    end
   
    
    methods
        function this = BiexponentialT1AbsGlobal
            % call superclass constructor
            this = this@Zone2Disp;
            this = this@DataFit;
            this.globalProcess = true;
        end
    end
    
    methods       
        % fill in the starting point of the model
        function this = evaluateStartPoint(this, xdata, ydata)  
            % here the pre-processing function has the role to define the
            % model
            ydata = abs(ydata);

        end %evaluateStartPoint

        function [res, new_data] = applyProcess(this, data)
            % check data and format them
            for n = numel(data)
                sze = size(data);
                xdata = data(n).x(data(n).mask);
                ydata = data(n).y(data(n).mask);
                dydata = data(n).dy(data(n).mask);
                if sze(2) > 1; xdata = xdata'; end
                if sze(2) > 1; ydata = ydata'; end
                if sze(2) > 1; dydata = dydata'; end
                
                % check parameter (minValue, maxValue,...) and format them
                this = evaluateStartPoint(this, xdata, ydata);
                % TO COMPLETE
                this.isFixed = logical(this.isFixed); %cast to logical
                
                lb = this.minValue(~this.isFixed);
                ub = this.maxValue(~this.isFixed);
                x0 = this.startPoint(~this.isFixed);
                
                % update function handle
                fun = setFixedParameter(this, this.modelHandle);
                
                % apply fit
                [res.bestValue, res.errorBar, res.gof] = applyFit(this.solver,...
                                        fun, xdata, ydata, dydata, x0, lb, ub);
                
                % gather data and make output structure
                new_data = formatFitData(this, res);            
    %             
            end
%             % update sub-models (if any)
%             this = updateSubModel(this);          
        end %applyProcess
    end

end

