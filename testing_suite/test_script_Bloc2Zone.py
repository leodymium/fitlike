# -*- coding: utf-8 -*-
"""
Created on Fri Feb 19 13:24:48 2021

@author: sambourg
"""

# TEST PERSO, MANIPULER LES DATA

import fitlike
from fitlike._lib.storage.DataUnit1D import DataUnit1D
from fitlike._lib.storage.DataUnit1D import Bloc
from fitlike._lib.storage.AcqProtocol import AcqProtocol
#from module.controller.LoadSdfFileV2 import LoadSdfFileV2
import sqlite3 as sl
import numpy as np
import matplotlib.pyplot as plt

from fitlike._lib.controller.data_import.load_sdf_v1 import LoadSdfV1
from fitlike._lib.controller.data_formating.Bloc2Zone_test import Bloc2Zone

"""
DESCRIPTION RAPIDE :
Ce prgramme est un moyen de s'assurer du bon fonctionnemnet de la classe Bloc2Zone
Elle compare deux méthode pour la construction de notre zone.
"""

# the code below is here for debugging purposes
#loader = LoadSdfFileV2('./datasets/Data samples Stelar v2/LS10 001N.sdf')

loader = LoadSdfV1('./datasets/Data samples Stelar v1/sample1 - tumour.sdf')
loader.database = sl.connect('my-test.db')
loader.import_sdf()

zone_id = 1                   # Indice de la zone que l'on souhaite étudier 

# Traitement manuel

print("Méthode brute")

BS = int(float(loader.protocol_list[zone_id].bs))
NBLK = int(float(loader.protocol_list[zone_id].nblk))
BRLX = float(loader.protocol_list[zone_id].brlx)
T1MX = float(loader.protocol_list[zone_id].t1mx)
BINI = 4*T1MX
BEND = 0.01*T1MX
TAU = np.zeros(NBLK)
mean = np.zeros(NBLK)
EWIP = 4
EWEP = 250

for k in range (NBLK):
    data_bloc = loader.bloc_list[k].x4
    mean[k] = np.mean(data_bloc[EWIP:EWEP])
for k in range (NBLK):
    TAU[k] = BINI*(BEND/BINI)**(float(k)/float(NBLK-1))


ZONE = [zone_id, BRLX, TAU, mean]
print("zone_id = ",ZONE[0])
print("B_relax = ",ZONE[1])
print("tau = \n",ZONE[2])
print("mean = \n",ZONE[3])
print("\n")


# Traitement avec class pour comparaison

print("Méthode classe")
Z = Bloc2Zone('./datasets/Data samples Stelar v1/sample1 - tumour.sdf')
Z.Zone(zone_id)
print("zone_id = ",Z.zone_id)
print("B_relax = ",Z.zone_Brelax)
print("tau = \n",Z.data_tau)
print("mean = \n",Z.data_mean)


# La figure n'a pas beaucoup d'intérêt
fig = plt.figure()
plt.plot(Z.data_tau,Z.data_mean)
plt.plot(ZONE[2],ZONE[3],'--')
plt.show()
