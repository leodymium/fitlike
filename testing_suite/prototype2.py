# -*- coding: utf-8 -*-
"""
Created on Tue Mar 16 19:56:12 2021

@author: Aime Labbe
"""

# -*- coding: utf-8 -*-
"""
Created on Tue Mar 16 16:00:33 2021

@author: Aime Labbe
"""
from scipy import signal
import numpy as np
import numpy.ma as ma
import matplotlib.pyplot as plt

import scipy.fft as fft


# Idée de l'algo : 
#   On cherche à classifier des points {y0,y1}_i de xi en deux dispersions.
#   Pour ce faire on va chercher à classifier les points par paires en xi et 
#   x(i+1) de manière à obtenir deux dispersions qui ont des dérivées secondes
#   minimales.
#
#   1) On attribue une qualité Q_i à chaque coordonnée xi. Pour chaque point 
#       xi, il y a 4 manières de le relier à ses plus proches voisins ((PPV).
# 
#        y0   a  b  c      Combinaisons     abc,   Abc,   AbC,   abC 
#        y1   A  B  C                       ABC,   aBC,   aBc,   ABc
#            x0  x1 x2                 j=    1      2      3      4
#
#       On pose D2_ij(b) : la dérivée seconde en xi pour la combinaison j 
#        qui suppose qu'on a b comme valeur centrale.
#
#       On pose Q_i = 1/( sqrt(  min_j( D2_ij(y0)^2 + D2_ij(y1)^2  ) ) )
#
#       En gros, si xi a une grande qualité, ça veut qu'il existe une manière 
#       de classifier xi et ses PPV selon deux droites.
#       
#       Enfin, si xi est en bord de domaine, on lui associe une qualité Qi->0
#
#   2) On attribue une qualité Qe_i à chaque segment reliant deux points xi, xi+1
#       On pose Qe_i = Q_i + Q_(i+1)
#
#   3) On trie la liste des segments en fonctions de leur qualité
#       -> Plus un segment est de grande qualité, plus on est sur de pouvoir 
#        classifier la paire de points qu'il relie
#   
#   4) On classifie les points par paires en parcourant la liste triée
#       On attribue un catégorie Ki à chaque point i ayant été relié ensemble.
#       Au départ, Ki=0 pout tout i, on pose k=1        
#
#       Deux cas possibles: 
#           i) Les deux points sont incompatibles
#               --------------------------------------------------------------
#               -Les Q_i et Q_(i+i) ont été obtenus pour des combinaisons qui
#               -s'excluent mutuellement     
#               -                            Q1    Q2
#               -                            j=1  j=2
#               -    ex : a  b  c  d     ->  abc  Bcd  -> Impossible de relier 
#               -         A  B  C  D         ABC  bCD  -> x1 et x2 car bc-Bc 
#               -        x0 x1 x2 x3         x1    x2  -> sont incompatibles
#               -------------------------------------------------------------
#               alors on ne classifie pas les points. 
#               Si Q2<Q1 alors on masque la valeur j=2 ayant servi à calculer
#               Q2 et on recalcule Q2. ex:
#                   Q_2 = 1/sqrt( min_(j!=2) ( D2_ij(y0)^2 + D2_ij(y1)^2 ))
#
#           ii) Les deux points sont compatibles. 
#               On vérifie s'il faut effectuer une permutation de {y0,y1}
#               pour relier xi à xj. 
#               --------------------------------------------------------------
#               - Exemple de permutation nécessaire     
#               -                            Q1    Q2
#               -                            j=1  j=2
#               -    ex : a  b  c  d     ->  abC  Bcd  -> Il faut permuter 
#               -         A  B  C  D         ABc  bCD  -> C/c (ou B/b) pour
#               -        x0 x1 x2 x3         x1    x2  -> relier x1 à x2
#               -------------------------------------------------------------
#
#               Alors cinq cas
#               a) Ki=K(i+1)=0 -> les points n'appartiennent à aucune catégorie
#                   On relie x(i+1) à x(i) et on associe la catégorie 
#                   Ki=Kj = k, on pose k+=1.
#                   On permute si nécessaire.
#               b) Ki!=0, K(i+1)=0
#                   On relie de deuxième au premier et on pose K(i+1)=Ki
#                   On permute si necessaire, sauf si xi est déjà permuté
#               c) Ki=0, K(i+1)!=0
#                   On relie le premier au deuxième et on pose Ki=K(i+1)
#                   On permute si necessaire, sauf si x(i+1) est déjà permuté
#               d) Ki != 0 != K(i+1) != Ki : deux catégories différentes
#                   On associe tous les points de K(i+1) à Ki=k. 
#                   On permute {y0,y1}_j pout tout j tq Kj=k si nessaire
#                   sauf si x(i+1) XOR xi sont déjà permutés
#                   
#               e) Ki=K(i+1)!=0 : même catégorie
#                   On ne fait rien
#
#   5) On répète (1) à (5) tant qu'il reste des segments non reliés.



##############################################################################
# Début de l'algo

# Nombre de points par dispersion
N = 101
#np.random.seed(1)
# Fonctions arbitraires utilisées pour simuler les deux dispersions
#     NB. Les fonctions n'ont aucun sens physique. 
def f(x):
    # fonctions arbitraires assez régulières
    def p1(x):
        return np.exp(-x/10)*(1 + x + x**2/100)
    def p2(x):
        return np.exp(-x/20)*(1 + x*.5 + x**2/150)*2
    def p3(x):
        return np.exp(-x/15)*(15 - x*0.8 + x**2/15 +(x/18)**5 )
    def p4(x):
        return np.exp(-x/4)*(1 + x*5 + x**2/175)*1
    y=np.array([p1(x)+p2(x), p3(x)+p4(x) ]) # Fonctions à départager
    for tmp in y.T:
        #pass
        np.random.shuffle(tmp)
    return y

x = np.linspace(0,75,N) # domaine
y = f(x) + np.random.normal(size=(2,N))*0.2 # 2 dispersion bruitées mélangées

# Affichage
plt.scatter(x, y[0])
plt.scatter(x, y[1])
plt.ylim((0,20))
plt.show()

# Une première métrique possible
dy = np.absolute( y[1]-y[0] ) / np.mean(y, axis=0) 
plt.plot(x,dy)

# dy[18:75] *= -1
# plt.plot(x,dy)
# plt.title("Difference map")
# plt.show()


# Calcul des dérivées secondes
d2 = np.zeros((N,4))
d2_mask = np.zeros((N,4), dtype=bool) # Va servir dans les cas «incompatibles»
d2 = ma.masked_array(d2, d2_mask)
for i in range(1,N-1):
    a,b,c,A,B,C = tuple( y[0,i-1:i+2] ) + tuple( y[1,i-1:i+2] )
    
    d2[i,0] = (c -2*b +a)**2 + (C -2*B +A)**2
    d2[i,1] = (C -2*b +a)**2 + (c -2*B +A)**2
    d2[i,2] = (c -2*b +A)**2 + (C -2*B +a)**2
    d2[i,3] = (C -2*b +A)**2 + (c -2*B +a)**2
d2[0,:] = d2.max()*2 # Bords
d2[-1,:]= d2.max()*2 # Bords

# Fonction pour voir si les points sont compatibles et s'il faut permuter
def check_case(i,j, fliped):
    # i,j les indices de d2 ayant servi au calcul des qualité des points xi
    # fliped : déjà permuté
    # En gros  : c'est une table de vérités
    if (i==0 or i==2) and (j==0 or j==1):
        ok = True # compatible
        to_flip=False
    elif (i==1 or i==3) and (j==2 or j==3):
        ok = True
        to_flip = True
    else:
        ok = False # Incompatible
        to_flip = False
    return ok, to_flip ^ fliped # ^ = XOR

# Fonction pour gérer les cas incompatibles
def clean_nodes(i0,i1,j0,j1):
    i,j = (i0,j0) if pts_quality[i0]<pts_quality[i1] else (i1,j1)
    
    d2_mask[i,j] = True
    d2_min[i] = np.min(d2[i])
    d2_minarg[i] = np.argmin(d2[i])
    
    pts_quality[i] = 1/d2_min[i]
    if  i<len(pts_quality)-1:
        edges_quality[i] = pts_quality[i]+pts_quality[i+1]


catMap = np.zeros( N )
fliped = np.zeros( N, dtype=bool )
catDict = {}
cat = 1
y_out = np.zeros_like(y)

# Deuxième métrique possible : carte d'incertitute
# Un point est d'autant plus incertain que les 4 combinaisons possibles 
# mènent à des dérivées secondes près les unes des autres.
# Bref, on pourrait relier les points n'importe comment
d2_sort = np.sort(d2, axis=1)    
uncertainty_map = np.absolute(
    np.mean(d2_sort, axis=1)/np.std(d2_sort,axis=1)-1)
# plt.ylim((0,   10)))
plt.plot(x,uncertainty_map)
plt.title("Uncertainty map")
plt.show()

d2_min = np.min(d2, axis=1)
d2_minarg=np.argmin(d2, axis=1)
pts_quality = 1/d2_min

edges_quality = pts_quality[1:]+pts_quality[:-1]
   
it=0
while True:
    # On crée la liste des edges qui n'ont pas encore été traités
    edges = []
    d2_min = np.min(d2, axis=1)
    for i in range(N-1):
        if catMap[i]==catMap[i+1]==0 or catMap[i]!=catMap[i+1]:
            edges.append([edges_quality[i],i])
    edges = sorted(edges, key=lambda edge: edge[0], reverse=True)
    
    if len(edges)==0: # Tous les ecges ont été reliés -> fin
        break
    
    for e in edges:
        i0,i1 = e[1], e[1]+1
        y0, y1 = y[:,i0], y[:,i1]
        j0, j1 = d2_minarg[i0], d2_minarg[i1]
        cat0, cat1 = catMap[i0], catMap[i1]
        
        _fliped = fliped[i0] ^ fliped[i1]
        ok, to_flip = check_case(j0, j1, _fliped)
        
        if not ok: # and i0!=0 and i1!=N-1:
            clean_nodes(i0,i1,j0,j1)
            
            # plt.scatter(x[i0], y[0,i0], c="k" )
            # plt.scatter(x[i0], y[1,i0], c="k" )
            # plt.scatter(x[i1], y[0,i1], c="k")
            # plt.scatter(x[i1], y[1,i1], c="k")
                            
        elif cat0==0 and cat1==0: # Points have no category yet
            catMap[i0] = cat
            catMap[i1] = cat
            catDict[cat] = [i0,i1]
            cat+=1
        
            y_out[:,i0] = y[:,i0]
            y_out[:,i1] = y[-1::-1,i1] if to_flip else y[:,i1]
            fliped[i1] = fliped[i1] ^ to_flip

        elif cat0!=0 and cat1==0: # Only 1st point has a category
            catMap[i1] = cat0
            catDict[cat0].append(i1)
            
            y_out[:,i1] = y[-1::-1,i1] if to_flip else y[:,i1]
            fliped[i1] = fliped[i1] ^ to_flip
            
        elif cat0==0 and cat1!=0: # Only 2nd point has a category
            catMap[i0] = cat1
            catDict[cat1].append(i0)
            
            y_out[:,i0] = y[-1::-1,i0] if to_flip else y[:,i0]
            fliped[i0] = fliped[i0] ^ to_flip

        elif cat0!=0 and cat1!=0 and cat0!=cat1 : # Both points have a category and they are not part of the same category
            IND = catDict.pop(cat1)
            catDict[cat0].extend(IND)
            for ind in IND:
                y_out[:,ind] = y_out[-1::-1,ind] if to_flip else y_out[:,ind]
                catMap[ind] = cat0
                fliped[ind] = fliped[ind] ^ to_flip
        # plt.scatter(x, y_out[0])
        # plt.scatter(x, y_out[1])
        
        # for CAT in catDict:
        #     ind = np.sort(catDict[CAT])
        #     plt.plot(x[ind],y_out[0,ind] ,"k", x[ind],y_out[1,ind], "k")
            
        # plt.scatter(x, fliped)
        # plt.ylim((0,20))
        # plt.show()
                
    if it>4:
        break
    it+=1


# plt.scatter(x, y[0])
# plt.scatter(x, y[1])
# plt.ylim((0,20))
# plt.show()    

y_out2=y_out.copy()


# Final clean-up
std = np.std(uncertainty_map)
mean = np.mean(uncertainty_map)

uu = ma.masked_array(uncertainty_map, uncertainty_map>mean+2*std)
std = np.std(uu)
mean = np.mean(uu)

index = np.argwhere(uncertainty_map > std*1.5)[:,0]

ii = np.argsort(-uncertainty_map[index] )
print(ii)
index = index[ii]+1


for i in index:    
    # plt.scatter(x, y_out[0])
    # plt.scatter(x, y_out[1])
    ffty0 = np.absolute( fft.fft( np.concatenate((y_out, y_out[:,::-1]), axis=1) ) )
    
    y_out[:,i:] = y_out[::-1,i:]
    
    # plt.scatter(x, y_out[0]+20, c="r")
    # plt.scatter(x, y_out[1]+20, c="y")
    ffty1 = np.absolute( fft.fft( np.concatenate((y_out, y_out[:,::-1]), axis=1) ) ) 
    
    t0 = abs( ffty0[0]+ffty0[1] )
    t1 = abs( ffty1[0]+ffty1[1] )
    
    n = int(N/2)*2
    xmean0 = sum(x[1:n]*np.sqrt( t0[1:n]))/sum(np.sqrt(t0[1:n]))
    xmean1 = sum(x[1:n]*np.sqrt(t1[1:n]))/sum(np.sqrt( t1[1:n]) )
    
    
    #res = np.sum( (ffty0-ffty1) > 0 ) - N #More high freq in 0
    res = np.sum( abs(t0)-abs(t1)< 0 ) -int(N/2)  
    res = -( xmean0-xmean1 )
    if res<0:
        fliped[i:] = fliped[i:] ^ True
        title="flipped"
    else:
        y_out[:,i:] = y_out[::-1,i:]
        title = "not flipped"
    print("index", i, ": res", res, "-->", title)
        
    # plt.title(title)
    # plt.scatter(x[i:i+1], y_out[0,i:i+1], c="k")
    # plt.scatter(x[i:i+1], y_out[1,i:i+1], c="k")
    # plt.ylim((0,40))
    # plt.show()
    
    # plt.plot(x[1:n], t0[1:n])
    # plt.plot(x[1:n], t1[1:n])
    # plt.legend([f"If flipped", "If not flipped"])
    # plt.title(title+f" {res:.2f}")
    # plt.show()
    
    

   
    

plt.title(title)
plt.scatter(x, y_out2[0], c="r")
plt.scatter(x, y_out2[1], c="b")
plt.scatter(x, y_out[0]+20, c="k" )
plt.scatter(x, y_out[1]+20,c="g")
plt.ylim((0,20))



plt.ylim((0,40))

plt.legend(["Avant final","Avant final","Après final","Après final" ])
plt.title("")


plt.title("Fin")
plt.show()

